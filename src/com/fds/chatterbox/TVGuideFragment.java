package com.fds.chatterbox;

import java.util.ArrayList;

import com.fds.chatterbox.adapters.ChannelRowData;
import com.fds.chatterbox.adapters.ExpandableAdapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;



public class TVGuideFragment extends Fragment{
	
	ExpandableAdapter adapter;
	View rootView;
	ExpandableListView lv;
	private ArrayList<ChannelRowData> parentItems = new ArrayList<ChannelRowData>();
	private ArrayList<Object> childItems = new ArrayList<Object>();

	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setGroupParents();
	    setChildData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		adapter = new ExpandableAdapter(getActivity().getApplicationContext(), parentItems, childItems);
	    adapter.setInflater(inflater, this.getActivity());
	    
	    return inflater.inflate(R.layout.tv_guide_fragment, container, false);  
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
	    super.onViewCreated(view, savedInstanceState);
	    
	    lv = (ExpandableListView) view.findViewById(R.id.exp_list);
	    lv.setAdapter(adapter);
	    lv.setGroupIndicator(null);

	}

	public void setGroupParents() {
		parentItems.add(new ChannelRowData("ערוץ 2", R.drawable.ic_launcher));
		parentItems.add(new ChannelRowData("ערוץ 10", R.drawable.ic_launcher));
	}

	public void setChildData() {

		// Channel 2
		ArrayList<String> child = new ArrayList<String>();
		child.add("המרוץ למיליון");
		child.add("מצב האומה");
		childItems.add(child);

		// Channel 10
		child = new ArrayList<String>();
		child.add("יורוליג");
		child.add("בלה בלה");
		childItems.add(child);

	}
}
