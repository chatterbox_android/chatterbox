package com.fds.chatterbox.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fds.chatterbox.R;

public class ExpandableAdapter extends BaseExpandableListAdapter{
	
	private Context mContext;
	private Activity activity;
	private ArrayList<Object> childtems;
	private LayoutInflater inflater;
	private ArrayList<ChannelRowData> parentItems;
	private ArrayList<String>child;
	
	private class ViewGroupHolder {
		public TextView mChannelName;
		public ImageView mChannelIcon;
	}
	
	public ExpandableAdapter(Context context, ArrayList<ChannelRowData> parents, ArrayList<Object> childern) {
		this.mContext = context;
		this.parentItems = parents;
		this.childtems = childern;
	}
	
	public void setInflater(LayoutInflater inflater, Activity activity) {
		this.inflater = inflater;
		this.activity = activity;
	}
	
	@Override
	public Object getChild(int arg0, int arg1) {
		return null;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		child = (ArrayList<String>) childtems.get(groupPosition);

		TextView textView = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.tv_program_row, null);
		}

		textView = (TextView) convertView.findViewById(R.id.program_name);
		textView.setText(child.get(childPosition));

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Toast.makeText(activity, child.get(childPosition),
						Toast.LENGTH_SHORT).show();
			}
		});

		return convertView;
	}


	@Override
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<String>) childtems.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int arg0) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return parentItems.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
    	View view = new View(mContext);
		ViewGroupHolder holder;
		
		if (null == convertView) {
			view = inflater.inflate(R.layout.tv_channel_row, null);
			
			// Create the view holder
			holder = new ViewGroupHolder();
			holder.mChannelName = (TextView) view.findViewById(R.id.channel_name);
			holder.mChannelIcon = (ImageView) view.findViewById(R.id.channel_icon);
			view.setTag(holder);
		}
		else {
			view = convertView;
			holder = (ViewGroupHolder)view.getTag();
		}
		
		ChannelRowData current = parentItems.get(groupPosition);
		holder.mChannelName.setText(current.getName());
		holder.mChannelIcon.setImageResource(current.getImage());
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}
	
	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}
}


