package com.fds.chatterbox.adapters;

public class ChannelRowData {
	private String name;
	private int image;
	
	public ChannelRowData(String name, int image){
		this.name = name;
		this.image = image;
	}
	
	public String getName(){
		return name;
	}
	
	public int getImage(){
		return image;
	}
}
